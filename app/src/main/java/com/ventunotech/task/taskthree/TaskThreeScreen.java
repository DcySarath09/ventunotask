package com.ventunotech.task.taskthree;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.ventunotech.task.R;
import com.ventunotech.task.base.BaseActivity;
import com.ventunotech.task.common.seekbar.GlideThumbnailTransformation;
import com.ventunotech.task.common.seekbar.PreviewBar;
import com.ventunotech.task.databinding.TaskthreeScreenBinding;

public class TaskThreeScreen extends BaseActivity {

    /*Bind View*/
    TaskthreeScreenBinding binding;
    String URL = "https://www.ventunotech.com/test/resource/Ayogya_SIS7__6DZC5SM8_JNKHTX3R_r2048.jpg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.taskthree_screen);

        binding.previewSeekBar.setMax(500000);

        /*Seekbar listner*/
        binding.previewSeekBar.addOnScrubListener(new PreviewBar.OnScrubListener() {
            @Override
            public void onScrubStart(PreviewBar previewBar) { }
            @Override
            public void onScrubMove(PreviewBar previewBar, int progress, boolean fromUser) {
                updateView(progress);
            }
            @Override
            public void onScrubStop(PreviewBar previewBar) { }
        });

    }

    /*Get thumbnail image*/
    private void updateView(int progress) {

        Glide.with(binding.videoView)
                .load(URL)
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .transform(new GlideThumbnailTransformation(progress))
                .into(binding.videoView);
    }

}
