package com.ventunotech.task.tasktwo;

import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;

import androidx.databinding.DataBindingUtil;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.ventunotech.task.R;
import com.ventunotech.task.base.BaseActivity;
import com.ventunotech.task.base.MyApplication;
import com.ventunotech.task.common.htmleditor.HtmlFormatter;
import com.ventunotech.task.common.htmleditor.HtmlFormatterBuilder;
import com.ventunotech.task.databinding.TasktwoScreenBinding;

import org.xml.sax.XMLReader;

import java.util.HashMap;
import java.util.Map;

public class TaskTwoScreen extends BaseActivity {

    //Bind View
    TasktwoScreenBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.tasktwo_screen);
        showBackArrow();

        //Call API
        if(isConnectingToInternet()){
            showProgressDialog();
            getHtmlText();
        }else{
            noInternetAlertDialog();
        }
    }

    private void getHtmlText() {

        String url ="https://www.ventunotech.com/test/resource/test_html.json";
        final Map<String, String> params = new HashMap<>();
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.e("RESPONSE FROM SERVER",""+ response);
                        Object result = null;
                        try {
                            if (response != null) {
                                Gson gson = new Gson();
                                result = gson.fromJson(response, HtmlResponse.class);
                                HtmlResponse htmlResponse = (HtmlResponse)result;
                                Spanned formattedHtml = HtmlFormatter.formatHtml(new HtmlFormatterBuilder().setHtml(htmlResponse.message));
                                binding.textView.setText(formattedHtml);

                            } else {
                                toast("Error");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgressDialog();
                        error.printStackTrace();
                        toast("Error");
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> param2 = new HashMap<String, String>();
                param2 = params;
                return param2;
            }
        };
        // Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(postRequest, "tag_html_req");
    }

    private class UItagHandler implements Html.TagHandler {
        @Override
        public void handleTag(boolean opening, String tag, Editable output, XMLReader xmlReader) {
            if(tag.equals("ul") && !opening) output.append("\n");
            if(tag.equals("li") && opening) output.append("\n\t•");
        }
    }
}
