package com.ventunotech.task.tasktwo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class HtmlResponse implements Serializable {

    @Expose
    @SerializedName("footer_text")
    public String footer_text;
    @Expose
    @SerializedName("url")
    public String url;
    @Expose
    @SerializedName("message")
    public String message;
    @Expose
    @SerializedName("title")
    public String title;
}
