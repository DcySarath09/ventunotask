package com.ventunotech.task.common.htmleditor;

import android.view.View;

import androidx.annotation.Nullable;


public interface OnClickATagListener {
    void onClick(View widget, @Nullable String href);

}
