package com.ventunotech.task.common.seekbar;

import android.widget.FrameLayout;


public interface PreviewAnimator {

    void move(FrameLayout previewView, PreviewBar previewBar);

    void show(FrameLayout previewView, PreviewBar previewBar);

    void hide(FrameLayout previewView, PreviewBar previewBar);

    void cancel(FrameLayout previewView, PreviewBar previewBar);

}
