package com.ventunotech.task.common.seekbar;


import android.widget.FrameLayout;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


public interface PreviewBar {

    int getProgress();

    int getMax();

    int getThumbOffset();

    int getScrubberColor();

    boolean isShowingPreview();

    boolean isPreviewEnabled();

    void setPreviewEnabled(boolean enabled);

    void showPreview();

    void hidePreview();

    void setAutoHidePreview(boolean autoHide);

    void setPreviewAnimator(@NonNull PreviewAnimator animator);

    void setPreviewAnimationEnabled(boolean enabled);

    void setPreviewLoader(@Nullable PreviewLoader previewLoader);

    void setPreviewThumbTint(@ColorInt int color);

    void setPreviewThumbTintResource(@ColorRes int colorResource);

    void attachPreviewView(@NonNull FrameLayout previewView);

    void addOnScrubListener(OnScrubListener listener);

    void removeOnScrubListener(OnScrubListener listener);

    void addOnPreviewVisibilityListener(OnPreviewVisibilityListener listener);

    void removeOnPreviewVisibilityListener(OnPreviewVisibilityListener listener);

    interface OnScrubListener {

        void onScrubStart(PreviewBar previewBar);

        void onScrubMove(PreviewBar previewBar, int progress, boolean fromUser);

        void onScrubStop(PreviewBar previewBar);
    }

    interface OnPreviewVisibilityListener {

        void onVisibilityChanged(PreviewBar previewBar, boolean isPreviewShowing);
    }


}
