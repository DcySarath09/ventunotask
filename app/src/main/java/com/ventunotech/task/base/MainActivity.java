package com.ventunotech.task.base;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.ventunotech.task.R;
import com.ventunotech.task.databinding.ActivityMainBinding;
import com.ventunotech.task.language.LanguageTask;
import com.ventunotech.task.taskone.TaskOneScreen;
import com.ventunotech.task.taskthree.TaskThreeScreen;
import com.ventunotech.task.tasktwo.TaskTwoScreen;
import com.ventunotech.task.thememode.ThemeModeScreen;

public class MainActivity extends BaseActivity {

    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setDefaultLanguage("en");
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main);

        binding.taskOneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this, TaskOneScreen.class));
            }
        });

        binding.taskTwoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this, TaskTwoScreen.class));
            }
        });


        binding.taskThreeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this, TaskThreeScreen.class));
            }
        });

        binding.taskFourBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this, ThemeModeScreen.class));
            }
        });

        binding.taskFiveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this, LanguageTask.class));
            }
        });
    }
}