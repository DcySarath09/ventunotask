package com.ventunotech.task.language;

import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.ventunotech.task.R;
import com.ventunotech.task.base.BaseActivity;
import com.ventunotech.task.databinding.LanguageTaskBinding;

import java.util.Locale;

public class LanguageTask extends BaseActivity {

    /*Bind View*/
    LanguageTaskBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.language_task);
        showBackArrow();
        setTitle("LanguageTask");

        /*check local language*/
        switch (Locale.getDefault().getDisplayLanguage()) {

            case "English":
                checkEnglishUI();
                break;

            case "தமிழ்":
                checkTamilUI();
                break;
        }

    }

    /*Set English*/
    public void setEnglish(View view) {
        setLanguage("en");
        checkEnglishUI();
    }

    /*Set Tamil*/
    public void setTamil(View view) {
        setLanguage("ta");
        checkTamilUI();
    }

    /*English UI changes*/
    private void checkTamilUI() {
        binding.englishBtn.setTextColor(getResources().getColor(R.color.colorPrimary));
        binding.englishBtn.setBackgroundColor(getResources().getColor(R.color.white));
        binding.tamilBtn.setTextColor(getResources().getColor(R.color.white));
        binding.tamilBtn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
    }

    /*Tamil UI Changes*/
    private void checkEnglishUI() {
        binding.englishBtn.setTextColor(getResources().getColor(R.color.white));
        binding.englishBtn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        binding.tamilBtn.setTextColor(getResources().getColor(R.color.colorPrimary));
        binding.tamilBtn.setBackgroundColor(getResources().getColor(R.color.white));
    }
}
