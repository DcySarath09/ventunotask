package com.ventunotech.task.taskone;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.ventunotech.task.R;
import com.ventunotech.task.base.BaseActivity;
import com.ventunotech.task.databinding.TaskoneScreenBinding;

import java.util.ArrayList;

public class TaskOneScreen extends BaseActivity {

    //Binding view
    TaskoneScreenBinding binding;

    //Create Objetcs
    GridListAdapter gridListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.taskone_screen);
        hideActionBar();

        //Create Grid View
        createGridView();

    }

    private void createGridView() {

        binding.itemsRecyclerView.setHasFixedSize(true);
        binding.itemsRecyclerView.setLayoutManager(new GridLayoutManager(this,2));
        ArrayList<String>strings = new ArrayList<>();
        gridListAdapter = new GridListAdapter(this,strings);
        binding.itemsRecyclerView.setAdapter(gridListAdapter);
        gridListAdapter.notifyDataSetChanged();
    }
}
