package com.ventunotech.task.taskone;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.ventunotech.task.R;

import java.util.ArrayList;

public class GridListAdapter extends RecyclerView.Adapter<GridListAdapter.ItemViewHolder> {

    Activity activity;
    private ArrayList<String> stringArrayList;

    public GridListAdapter(Activity activity, ArrayList<String> stringArrayList) {
        this.activity = activity;
        this.stringArrayList = stringArrayList;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        public ItemViewHolder(View view) {
            super(view);
            //categoryImageView = (ImageView) view.findViewById(R.id.categoryImageView);
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_item_list, parent, false);
        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder viewHolder, final int i) {


    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public int getItemCount() {
        return 20;
    }

}